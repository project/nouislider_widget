## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

This module provides a slider widget for integer fields, having a simpler approach and avoiding jQuery dependency.

## REQUIREMENTS

- [noUiSlider](https://github.com/leongersen/noUiSlider)

## INSTALLATION

  * Install normally as other modules are installed. For support:
    https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules


## CONFIGURATION

  * For the content edit form you will find the settings alonside with other widget's settings on:
    _/admin/structure/types/manage/[CONTENT_TYPE]/form-display_

## MAINTAINERS

Current maintainers:
 * Ivan Duarte (jidrone) - https://www.drupal.org/u/jidrone
