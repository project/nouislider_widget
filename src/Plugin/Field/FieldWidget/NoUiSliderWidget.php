<?php

namespace Drupal\nouislider_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;

/**
 * Defines the 'nouislider_widget_widget' field widget.
 *
 * @FieldWidget(
 *   id = "nouislider_widget_widget",
 *   label = @Translation("noUiSlider"),
 *   field_types = {"integer"},
 * )
 */
class NoUiSliderWidget extends NumberWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'step' => 1,
      'tooltip' => TRUE,
      'hide_input' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element['step'] = [
      '#type' => 'number',
      '#title' => $this->t('Step'),
      '#min' => 1,
      '#required' => TRUE,
      '#default_value' => $this->getSetting('step'),
    ];
    $element['tooltip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Tooltip'),
      '#default_value' => $this->getSetting('tooltip'),
    ];
    $element['hide_input'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Input'),
      '#default_value' => $this->getSetting('hide_input'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Step: @step', ['@step' => $this->getSetting('step')]);
    $summary[] .= $this->t('Show Tooltip: @tooltip', [
      '@tooltip' => $this->getSetting('tooltip') ? $this->t('Yes') : $this->t('No'),
    ]);
    $summary[] .= $this->t('Hide Input: @hide_input', [
      '@hide_input' => $this->getSetting('hide_input') ? $this->t('Yes') : $this->t('No'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'nouislider_widget/widget';
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['value']['#attributes']['readonly'] = "readonly";
    $element['value']['#attributes']['class'][] = 'nouislider-input';

    // Hide input.
    if ($this->getSetting('hide_input')) {
      $element['value']['#attributes']['class'][] = 'visually-hidden';
    }
    // Add slider container.
    $element['slider'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['nouislider-range'],
        'data-tooltip' => $this->getSetting('tooltip'),
        'data-step' => $this->getSetting('step'),
      ],
    ];
    return $element;
  }

}
