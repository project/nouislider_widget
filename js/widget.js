'use strict';

(function (Drupal) {
  Drupal.behaviors.noUiSliderWidget = {
    attach: function attach(context) {
      var fields = context.querySelectorAll('.field--widget-nouislider-widget-widget');
      fields.forEach(function (field) {
        var input = field.querySelector('input');
        var range = field.querySelector('.nouislider-range');
        // Getting suffix value.
        var suffix = field.querySelector('.field-suffix');
        var suffixValue = false;
        if (suffix) {
          var suffixValue = suffix.textContent;
        }
        // Hiding prefix if needed.
        if (input.classList.contains('visually-hidden')) {
          var prefix = field.querySelector('.field-prefix');
          if (prefix) {
            prefix.classList.add('visually-hidden');
          }
        }
        // Getting values for basic config.
        var min = parseInt(input.getAttribute("min")) || 0;
        var max = parseInt(input.getAttribute("max")) || (min + 100);
        var step = parseInt(range.dataset.step) || 1;
        var value = parseInt(input.value) || min;
        var slider = noUiSlider.create(range, {
          range: {
            'min': min,
            'max': max
          },
          tooltips: Boolean(parseInt(range.dataset.tooltip)),
          connect: 'lower',
          start: value,
          step: step,
          format: {
            to: function (value) {
              if (suffixValue) {
                return value + suffixValue;
              }
              return Number(value);
            },
            from: function (value) {
              if (suffixValue) {
                return Number(value.replace(suffixValue, ''));
              }
              return Number(value);
            }
          }
        });
        slider.on('update', function (values, handle) {
          if (suffixValue) {
            values[handle] = Number(values[handle].replace(suffixValue, ''));
          }
          input.value = values[handle];
        });
      });
    }
  };
})(Drupal);
